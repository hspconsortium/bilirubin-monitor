package org.hspconsortium.bilirubin.monitor.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

public class EmailService {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmailService.class);

    public static void sendEmail(final String username, final String password, String to, String from, String subject, String message) {

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });

        try {

            Message emailMessage = new MimeMessage(session);
            emailMessage.setFrom(new InternetAddress(from));
            emailMessage.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
            emailMessage.setSubject(subject);
            String html = "<html><body>"
                    + "<form action='TODO' method='post' target='_blank'>"
                    + "<h4>Warning Warning Warning<b></h4>"
                    + "<br/><br/><br/>"
                    + message
                    + "<br/><br/><br/>"
                    + "<input type='submit' value='Open Bilirubin Application' />&nbsp;</form>"
                    + "</form>"
                    + "</body></html>";
            emailMessage.setContent(html, "text/html; charset=utf-8");

            Transport.send(emailMessage);

            LOGGER.info("Email message sent");

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }
}