package org.hspconsortium.bilirubin.monitor.service;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.model.primitive.UriDt;
import org.hl7.fhir.dstu3.model.*;
import org.hspconsortium.bilirubin.monitor.rule.BilirubinNotFoundWhenRequiredMatcher;
import org.hspconsortium.bilirubin.monitor.rule.BilirubinThresholdExceededMatcher;
import org.hspconsortium.bilirubin.monitor.rule.PatientIsInCriticalAgeRangeMatcher;
import org.hspconsortium.client.auth.credentials.Credentials;
import org.hspconsortium.client.session.clientcredentials.ClientCredentialsSessionFactory;
import org.hspconsortium.platform.messaging.converter.ResourceStringConverter;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public interface PatientViewCdsHooksService {

    String health();

    String echo(String payload);

    String doPatientViewCdsHooks(String inParamsStr);

    @Service
    class Impl implements PatientViewCdsHooksService {

        public static final String BILIRUBIN_CODE = "58941-6";

        @Inject
        ClientCredentialsSessionFactory<? extends Credentials> ehrSessionFactory;

        @Inject
        PatientIsInCriticalAgeRangeMatcher patientIsInCriticalAgeRangeMatcher;

        @Inject
        BilirubinNotFoundWhenRequiredMatcher bilirubinNotFoundWhenRequiredMatcher;

        @Inject
        BilirubinThresholdExceededMatcher bilirubinThresholdExceededMatcher;

        @Inject
        ResourceStringConverter resourceStringConverter;

        private String emptyParameterResponse;

        private String getEmptyParameterResponse() {
            if (emptyParameterResponse == null) {
                emptyParameterResponse = resourceStringConverter.toString(new Parameters());
            }
            return emptyParameterResponse;
        }

        @Override
        public String health() {
            return ehrSessionFactory != null ? "OK" : "Not Initialized";
        }

        @Override
        public String echo(String payload) {
            return payload;
        }

        @Override
        public String doPatientViewCdsHooks(String inParamsStr) {
            Parameters in = (Parameters) resourceStringConverter.toResource(inParamsStr);
            Date now = new Date();
            Patient patient = null;
            List<Observation> bilirubinObservations = new LinkedList<>();
            UriType fhirServerUri = null;
            for (Parameters.ParametersParameterComponent parameter : in.getParameter()) {
                if ("preFetchData".equals(parameter.getName())) {
                    Bundle bundle = (Bundle) parameter.getResource();
                    patient = findPatientInBundle(bundle);
                } else if ("fhirServer".equals(parameter.getName())) {
                    fhirServerUri = (UriType) parameter.getValue();
                }
            }
            bilirubinObservations = findBilirubinObservations(fhirServerUri, patient);
            return resourceStringConverter.toString(evaluate(patient, bilirubinObservations, now));
        }

        private Patient findPatientInBundle(Bundle bundle) {
            for (Bundle.BundleEntryComponent entry : bundle.getEntry()) {
                Object resource = entry.getResource();
                if (resource instanceof Patient) {
                    return (Patient) resource;
                } else if (resource instanceof Bundle) {
                    Patient patient = findPatientInBundle((Bundle) resource);
                    if (patient != null) {
                        return patient;
                    }
                }
            }
            return null;
        }

        private List<Observation> findBilirubinObservations(UriType fhirServerUri, Patient patient) {
            return Collections.emptyList();
        }

        // todo should be able to return multiple cards
        private Parameters evaluate(Patient patient, List<Observation> bilirubinObservations, Date now) {
            String hi = "![alt text](http://52.70.147.29/images/NantHealthLogo.png)<br>Coverage for Daniel Adams is ACTIVE with Aries Health Plan, effective 2016-01-01";
            // is this a newborn baby?  < 120 hours
            if (patient != null) {
                if (patientIsInCriticalAgeRangeMatcher.matches(now, patient.getBirthDate())) {
                    // todo these should not be if-then-else, but should all be executed individually
                    if (bilirubinNotFoundWhenRequiredMatcher.matches(now, patient.getBirthDate(), bilirubinObservations)) {
                        return createCard("No Bilirubin Observation Found", "The patient must be evaluated for elevated bilirubin levels", "Perform Bilirubin Test");
                    } else if (bilirubinThresholdExceededMatcher.matches(now, patient.getBirthDate(), bilirubinObservations)) {
                        return createCard("Bilirubin Levels Critical", "The patient has elevated bilirubin levels.", "Order Bilirubin Lights");
                    }
                }
            }
            // for demo purposes, show card anyway
            return createCard("Bilirubin Levels Critical", "The patient has elevated bilirubin levels.", "Order Bilirubin Lights");
        }

        private Parameters createCard(String summary, String detail, String suggestion) {
            // no Bilirubin observations recorded
            Parameters out = new Parameters();
            Parameters.ParametersParameterComponent cardParameter = out.addParameter().setName("card");
            Parameters.ParametersParameterComponent summaryPart = cardParameter.addPart().setName("summary").setValue(new StringType(summary));
            Parameters.ParametersParameterComponent detailsPart = cardParameter.addPart().setName("detail").setValue(new StringType(detail));

            Parameters.ParametersParameterComponent sourcePart = cardParameter.addPart().setName("source");
            sourcePart.addPart().setName("label").setValue(new StringType("HSPC Bilirubin Monitor"));
//            sourcePart.addPart().setName("url").setValue(new UriDt("http://www.ksl.com"));

            Parameters.ParametersParameterComponent suggestionPart = cardParameter.addPart().setName("suggestion");
            suggestionPart.addPart().setName("label").setValue(new StringType(suggestion));

            Parameters.ParametersParameterComponent indicatorPart = cardParameter.addPart().setName("indicator").setValue(new CodeType("warning"));

//            Parameters.ParametersParameterComponent linkPart = cardParameter.addPart().setName("link");
//            linkPart.addPart().setName("label").setValue(new StringDt("Open Bilirubin Chart"));
//            linkPart.addPart().setName("url").setValue(new UriDt("https://sandbox.hspconsortium.org/hspc-bilirubin-risk-chart/static/bilirubin-chart"));
            FhirContext FHIR_CONTEXT = FhirContext.forDstu3();
            System.out.println(FHIR_CONTEXT.newJsonParser().encodeResourceToString(out));
            return out;
        }
    }
}
