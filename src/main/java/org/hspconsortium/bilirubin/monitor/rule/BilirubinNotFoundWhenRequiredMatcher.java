package org.hspconsortium.bilirubin.monitor.rule;

import org.hl7.fhir.dstu3.model.Observation;
import org.springframework.context.annotation.Configuration;

import java.util.Date;
import java.util.List;

@Configuration
public class BilirubinNotFoundWhenRequiredMatcher {

    /**
     * @param now       Passed as an argument for consistent values
     * @param birthDate patient birthdate
     * @return
     */
    public boolean matches(Date now, Date birthDate, List<Observation> observations) {
        return
                // mocked
                observations.isEmpty();
    }
}
