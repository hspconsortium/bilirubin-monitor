package org.hspconsortium.bilirubin.monitor.rule;

import org.springframework.context.annotation.Configuration;

import java.util.Date;

@Configuration
public class PatientIsInCriticalAgeRangeMatcher {

    public static final long FIVE_DAYS_MILLIS = 5 * 24 * 60 * 60 * 1000;

    /**
     * @param now       Passed as an argument for consistent values
     * @param birthDate patient birthdate
     * @return
     */
    public boolean matches(Date now, Date birthDate) {

        return
                // has been born (in the past)
                now.getTime() > birthDate.getTime()
                        // is not over 5 days old
                        && now.getTime() < (birthDate.getTime() + FIVE_DAYS_MILLIS);
    }
}
